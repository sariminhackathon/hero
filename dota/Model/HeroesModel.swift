//
//  HeroesModel.swift
//  dota
//
//  Created by Oscar Perdanakusuma Adipati on 14/04/20.
//  Copyright © 2020 Sarimin App. All rights reserved.
//

import ObjectMapper
import Foundation

struct HeroStats {
    let localized_name: String
    let img: String
    let primary_attr: String
    let attack_type: String
    let base_health: String
    let base_attack_max: String
    let move_speed: String
    let base_mana: String
    let roles: [String]
}
