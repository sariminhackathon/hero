//
//  PromoCellView.swift
//  kemala
//
//  Created by Oscar Perdanakusuma Adipati on 03/04/20.
//  Copyright © 2020 Sarimin App. All rights reserved.
//

import Foundation
import UIKit
import Kingfisher

class PromoCellView: UITableViewCell {
    
    @IBOutlet weak var lblProductName: UILabel!
    @IBOutlet weak var imgProduct: UIImageView!
    @IBOutlet weak var promoView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
}

class PromoDashboardCellView: UICollectionViewCell {
    
    @IBOutlet weak var lblProductName: UILabel!
    @IBOutlet weak var imgProduct: UIImageView!
    @IBOutlet weak var promoView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
}
