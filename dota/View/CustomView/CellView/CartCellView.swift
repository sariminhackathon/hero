//
//  CartCellView.swift
//  kemala
//
//  Created by Oscar Perdanakusuma Adipati on 11/04/20.
//  Copyright © 2020 Sarimin App. All rights reserved.
//

import Foundation
import UIKit
import Kingfisher

class CartCellView: UITableViewCell {
    
    @IBOutlet weak var lblProductName: UILabel!
    @IBOutlet weak var lblProductQuantity: UILabel!
    @IBOutlet weak var lblProductSize: UILabel!
    @IBOutlet weak var lblProductPrice: UILabel!
    @IBOutlet weak var lblDelete: UILabel!
    @IBOutlet weak var cartView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
}
