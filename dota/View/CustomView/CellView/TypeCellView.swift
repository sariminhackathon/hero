//
//  TypeCellView.swift
//  dota
//
//  Created by Oscar Perdanakusuma Adipati on 14/04/20.
//  Copyright © 2020 Sarimin App. All rights reserved.
//

import Foundation
import UIKit
import Kingfisher

class TypeCellView: UICollectionViewCell {
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var imgType: UIImageView!
    @IBOutlet weak var typeView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
}

