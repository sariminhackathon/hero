//
//  ProductCellView.swift
//  kemala
//
//  Created by Oscar Perdanakusuma Adipati on 10/04/20.
//  Copyright © 2020 Sarimin App. All rights reserved.
//

import Foundation
import UIKit
import Kingfisher

class SimilarProductCellView: UICollectionViewCell {
    
    @IBOutlet weak var lblProductName: UILabel!
    @IBOutlet weak var imgProduct: UIImageView!
    @IBOutlet weak var productView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
}

class ProductListCellView: UICollectionViewCell {
    
    @IBOutlet weak var lblProductName: UILabel!
    @IBOutlet weak var lblProductSize: UILabel!
    @IBOutlet weak var lblProductPrice: UILabel!
    @IBOutlet weak var lblTotalStock: UILabel!
    @IBOutlet weak var imgProduct: UIImageView!
    @IBOutlet weak var productListView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
}

class ProductSalesCellView: UITableViewCell {
    
    @IBOutlet weak var lblProductName: UILabel!
    @IBOutlet weak var lblProductQuantity: UILabel!
    @IBOutlet weak var lblProductSize: UILabel!
    @IBOutlet weak var lblProductPrice: UILabel!
    @IBOutlet weak var lblSalesDate: UILabel!
    @IBOutlet weak var promoView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
}

