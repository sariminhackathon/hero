//
//  CategoryCellView.swift
//  kemala
//
//  Created by Oscar Perdanakusuma Adipati on 04/04/20.
//  Copyright © 2020 Sarimin App. All rights reserved.
//

import Foundation
import UIKit
import Kingfisher

class CategoryCellView: UICollectionViewCell {
    
    @IBOutlet weak var lblCategory: UILabel!
    @IBOutlet weak var imgCategory: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
}
