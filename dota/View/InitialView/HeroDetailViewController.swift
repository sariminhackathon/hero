//
//  HeroDetailViewController.swift
//  dota
//
//  Created by Oscar Perdanakusuma Adipati on 14/04/20.
//  Copyright © 2020 Sarimin App. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import AlamofireObjectMapper
import SwiftyJSON
import Kingfisher
import CoreData

class HeroDetailViewController: BaseParentController {
    @IBOutlet weak var heroImage: UIImageView!
    @IBOutlet weak var lblType: UILabel!
    @IBOutlet weak var lblAttr: UILabel!
    @IBOutlet weak var lblHealth: UILabel!
    @IBOutlet weak var lblMaxAttack: UILabel!
    @IBOutlet weak var lblSpeed: UILabel!
    @IBOutlet weak var lblRoles: UITextView!
    @IBOutlet weak var typeView: UIView!
    @IBOutlet weak var typeCollectionView: UICollectionView!
    var typeIDs = [String]()
    var heroNamesArray = [String]()
    var heroImagesArray = [String]()
    var typeArray = [String]()
    var attrArray = [String]()
    var healthArray = [String]()
    var maxAttackArray = [(Int,Int)]()
    var speedArray = [(Int,Int)]()
    var manaArray = [(Int,Int)]()
    var rolesArray = [[String]]()
    var herosImage: String?
    var types: String?
    var attr: String?
    var health: String?
    var maxAttack: String?
    var speed: String?
    var roles: [String]?
    var newtypeIDs = [String]()
    var newheroNamesArray = [String]()
    var newheroImagesArray = [String]()
    var newtypeArray = [String]()
    var newattrArray = [String]()
    var newhealthArray = [String]()
    var newmaxAttackArray = [String]()
    var newspeedArray = [String]()
    var newmanaArray = [String]()
    var newrolesArray = [[String]]()
    var posSpeedArray = [Int]()
    var posAttackArray = [Int]()
    var posManaArray = [Int]()
    var coreHeroList: [NSManagedObject] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        lblType.text = types!
        lblAttr.text = attr!
        lblHealth.text = health!
        lblMaxAttack.text = maxAttack!
        lblSpeed.text = speed!
        lblRoles.text = roles!.joined(separator: "\n")
        let url = URL(string: herosImage ?? "")
        heroImage.kf.setImage(with: url)
//        heroImage.downloaded(from: herosImage ?? "")
        heroImage.clipsToBounds = true
        heroImage.layer.cornerRadius = heroImage.frame.height / 2
        heroImage.contentMode = .scaleAspectFill
        
        setEnableTapGestureOnMainView(isEnable: false)
        
        typeCollectionView.dataSource = self
        typeCollectionView.delegate = self
        typeCollectionView.register(UINib(nibName: "TypeView", bundle: nil), forCellWithReuseIdentifier: "typeCell")
        
        if(isConnectingInternet()==false){
            AlertViewUtil.sharedIns.showAlertFeedback(self, messageText: Wordings.msg_internet_error, actionButton1: Wordings.BTN_CLOSE, actionButton2: "")
            return
        } else {
            callAPIHeroes()
        }
    }
    
    func callAPIHeroes(){
        typeIDs.removeAll()
        heroNamesArray.removeAll()
        heroImagesArray.removeAll()
        typeArray.removeAll()
        attrArray.removeAll()
        healthArray.removeAll()
        maxAttackArray.removeAll()
        speedArray.removeAll()
        manaArray.removeAll()
        rolesArray.removeAll()
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        let managedContext = appDelegate.persistentContainer.viewContext
        let fetchRequestHeroes = NSFetchRequest<NSManagedObject>(entityName: "Hero")
        do {
            coreHeroList = try managedContext.fetch(fetchRequestHeroes)
            if (coreHeroList.count > 0) {
                for hero in coreHeroList {
                    let heroID = hero.value(forKey: "heroID") as? String ?? ""
                    let attackMax = hero.value(forKey: "baseMaxAttack") as? String ?? ""
                    let speed = hero.value(forKey: "moveSpeed") as? String ?? ""
                    let mana = hero.value(forKey: "baseMana") as? String ?? ""
                    self.maxAttackArray.append((Int(attackMax) ?? 0,Int(heroID) ?? 0))
                    self.speedArray.append((Int(speed) ?? 0,Int(heroID) ?? 0))
                    self.manaArray.append((Int(mana) ?? 0,Int(heroID) ?? 0))
                }
                self.newArray()
            } else {
                let url = NSURL(string:Constant.getAPIHeroes())
                print("HEROES URL:\(String(describing: url!))")
                showProgressDialog(message: Wordings.msg_please_wait)

                Alamofire.request(Constant.getAPIHeroes(), method : .get, parameters: nil).responseJSON { response in
                    self.dismissProgressDialog()
                    switch(response.result) {
                    case .success(_):
                        if response.result.value != nil {
                            let json = JSON(response.result.value as Any)
                            let statusCode = response.response?.statusCode
                            print("statusCode:\(String(describing: statusCode))")
                            print("data:\(json)")
                            if ((json.count) > 0) {
                                for i in 0..<(json.count)  {
                                    let heroID = json[i]["id"].stringValue
                                    let attackMax = json[i]["base_attack_max"].stringValue
                                    let speed = json[i]["move_speed"].stringValue
                                    let mana = json[i]["base_mana"].stringValue
                                    self.maxAttackArray.append((Int(attackMax) ?? 0,Int(heroID) ?? 0))
                                    self.speedArray.append((Int(speed) ?? 0,Int(heroID) ?? 0))
                                    self.manaArray.append((Int(mana) ?? 0,Int(heroID) ?? 0))
                                }
                                self.newArray()
                            }
                        }
                    case .failure(_):
                        AlertViewUtil.sharedIns.showAlertFeedback(self, messageText: "Failed get heroes data", actionButton1: Wordings.BTN_CLOSE, actionButton2: "")
                    }
                }
            }
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
    }
    
    func newArray() {
        posManaArray.removeAll()
        posSpeedArray.removeAll()
        posAttackArray.removeAll()
        maxAttackArray.sort {
            return $1.0 < $0.0
        }
        manaArray.sort {
            return $1.0 < $0.0
        }
        speedArray.sort {
            return $1.0 < $0.0
        }
        print("sortedspeedArray:\(speedArray)")
        let firstMaxAttack = maxAttackArray[0].1
        let secondMaxAttack = maxAttackArray[1].1
        let thirdMaxAttack = maxAttackArray[2].1
        let firstMaxMana = manaArray[0].1
        let secondMaxMana = manaArray[1].1
        let thirdMaxMana = manaArray[2].1
        let firstMaxSpeed = speedArray[0].1
        let secondMaxSpeed = speedArray[1].1
        let thirdMaxSpeed = speedArray[2].1
        if self.attr == "agi" {
            posSpeedArray.append(firstMaxSpeed)
            posSpeedArray.append(secondMaxSpeed)
            posSpeedArray.append(thirdMaxSpeed)
            print("newArraynya:\(posSpeedArray)")
        } else if self.attr == "str" {
            posAttackArray.append(firstMaxAttack)
            posAttackArray.append(secondMaxAttack)
            posAttackArray.append(thirdMaxAttack)
            print("newArraynya:\(posAttackArray)")
        } else if self.attr == "int" {
            posManaArray.append(firstMaxMana)
            posManaArray.append(secondMaxMana)
            posManaArray.append(thirdMaxMana)
            print("newArraynya:\(posManaArray)")
        }
        callAPISimilarHeroes()
    }
    
    func callAPISimilarHeroes(){
        newtypeIDs.removeAll()
        newheroNamesArray.removeAll()
        newheroImagesArray.removeAll()
        newtypeArray.removeAll()
        newattrArray.removeAll()
        newhealthArray.removeAll()
        newmaxAttackArray.removeAll()
        newspeedArray.removeAll()
        newmanaArray.removeAll()
        newrolesArray.removeAll()
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        let managedContext = appDelegate.persistentContainer.viewContext
        let fetchRequestHeroes = NSFetchRequest<NSManagedObject>(entityName: "Hero")
        do {
            coreHeroList = try managedContext.fetch(fetchRequestHeroes)
            if (coreHeroList.count > 0) {
                for hero in coreHeroList {
                    let heroID = hero.value(forKey: "heroID") as? String ?? ""
                    let heroName = hero.value(forKey: "heroName") as? String ?? ""
                    let heroAttr = hero.value(forKey: "primaryAttr") as? String ?? ""
                    let attackType = hero.value(forKey: "attackType") as? String ?? ""
                    let heroImage = hero.value(forKey: "heroImage") as? String ?? ""
                    let health = hero.value(forKey: "baseHealth") as? String ?? ""
                    let attackMax = hero.value(forKey: "baseMaxAttack") as? String ?? ""
                    let speed = hero.value(forKey: "moveSpeed") as? String ?? ""
                    let mana = hero.value(forKey: "baseMana") as? String ?? ""
                    let roles = hero.value(forKey: "roles") as? [String] ?? [""]
                    if self.attr == "agi" {
                        for singleID in self.posSpeedArray {
                            if Int(heroID) == singleID {
                                self.newtypeIDs.append(heroID)
                                self.newheroNamesArray.append(heroName)
                                self.newheroImagesArray.append(heroImage)
                                self.newtypeArray.append(attackType)
                                self.newattrArray.append(heroAttr)
                                self.newhealthArray.append(health)
                                self.newmaxAttackArray.append(attackMax)
                                self.newspeedArray.append(speed)
                                self.newmanaArray.append(mana)
                                self.newrolesArray.append(roles)
                            }
                        }
                    } else if self.attr == "str" {
                        for singleID in self.posAttackArray {
                            if Int(heroID) == singleID {
                                self.newtypeIDs.append(heroID)
                                self.newheroNamesArray.append(heroName)
                                self.newheroImagesArray.append(heroImage)
                                self.newtypeArray.append(attackType)
                                self.newattrArray.append(heroAttr)
                                self.newhealthArray.append(health)
                                self.newmaxAttackArray.append(attackMax)
                                self.newspeedArray.append(speed)
                                self.newmanaArray.append(mana)
                                self.newrolesArray.append(roles)
                            }
                        }
                    } else if self.attr == "int" {
                        for singleID in self.posManaArray {
                            if Int(heroID) == singleID {
                                self.newtypeIDs.append(heroID)
                                self.newheroNamesArray.append(heroName)
                                self.newheroImagesArray.append(heroImage)
                                self.newtypeArray.append(attackType)
                                self.newattrArray.append(heroAttr)
                                self.newhealthArray.append(health)
                                self.newmaxAttackArray.append(attackMax)
                                self.newspeedArray.append(speed)
                                self.newmanaArray.append(mana)
                                self.newrolesArray.append(roles)
                            }
                        }
                    } else {
                        self.newtypeIDs.append(heroID)
                        self.newheroNamesArray.append(heroName)
                        self.newheroImagesArray.append(heroImage)
                        self.newtypeArray.append(attackType)
                        self.newattrArray.append(heroAttr)
                        self.newhealthArray.append(health)
                        self.newmaxAttackArray.append(attackMax)
                        self.newspeedArray.append(speed)
                        self.newmanaArray.append(mana)
                        self.newrolesArray.append(roles)
                    }
                }
                self.typeCollectionView.reloadData()
            } else {
                let url = NSURL(string:Constant.getAPIHeroes())
                print("HEROES URL:\(String(describing: url!))")

                Alamofire.request(Constant.getAPIHeroes(), method : .get, parameters: nil).responseJSON { response in
                    switch(response.result) {
                    case .success(_):
                        if response.result.value != nil {
                            let json = JSON(response.result.value as Any)
                            let statusCode = response.response?.statusCode
                            print("statusCode:\(String(describing: statusCode))")
                            print("data:\(json)")
                            if ((json.count) > 0) {
                                for i in 0..<(json.count)  {
                                    let heroID = json[i]["id"].stringValue
                                    let heroName = json[i]["localized_name"].stringValue
                                    let heroAttr = json[i]["primary_attr"].stringValue
                                    let attackType = json[i]["attack_type"].stringValue
                                    let heroImage = json[i]["img"].stringValue
                                    let health = json[i]["base_health"].stringValue
                                    let attackMax = json[i]["base_attack_max"].stringValue
                                    let speed = json[i]["move_speed"].stringValue
                                    let mana = json[i]["base_mana"].stringValue
                                    let roles = json[i]["roles"].arrayObject as! [String]
                                    if self.attr == "agi" {
                                        for singleID in self.posSpeedArray {
                                            if Int(heroID) == singleID {
                                                self.newtypeIDs.append(heroID)
                                                self.newheroNamesArray.append(heroName)
                                                self.newheroImagesArray.append(heroImage)
                                                self.newtypeArray.append(attackType)
                                                self.newattrArray.append(heroAttr)
                                                self.newhealthArray.append(health)
                                                self.newmaxAttackArray.append(attackMax)
                                                self.newspeedArray.append(speed)
                                                self.newmanaArray.append(mana)
                                                self.newrolesArray.append(roles)
                                            }
                                        }
                                    } else if self.attr == "str" {
                                        for singleID in self.posAttackArray {
                                            if Int(heroID) == singleID {
                                                self.newtypeIDs.append(heroID)
                                                self.newheroNamesArray.append(heroName)
                                                self.newheroImagesArray.append(heroImage)
                                                self.newtypeArray.append(attackType)
                                                self.newattrArray.append(heroAttr)
                                                self.newhealthArray.append(health)
                                                self.newmaxAttackArray.append(attackMax)
                                                self.newspeedArray.append(speed)
                                                self.newmanaArray.append(mana)
                                                self.newrolesArray.append(roles)
                                            }
                                        }
                                    } else if self.attr == "int" {
                                        for singleID in self.posManaArray {
                                            if Int(heroID) == singleID {
                                                self.newtypeIDs.append(heroID)
                                                self.newheroNamesArray.append(heroName)
                                                self.newheroImagesArray.append(heroImage)
                                                self.newtypeArray.append(attackType)
                                                self.newattrArray.append(heroAttr)
                                                self.newhealthArray.append(health)
                                                self.newmaxAttackArray.append(attackMax)
                                                self.newspeedArray.append(speed)
                                                self.newmanaArray.append(mana)
                                                self.newrolesArray.append(roles)
                                            }
                                        }
                                    } else {
                                        self.newtypeIDs.append(heroID)
                                        self.newheroNamesArray.append(heroName)
                                        self.newheroImagesArray.append(heroImage)
                                        self.newtypeArray.append(attackType)
                                        self.newattrArray.append(heroAttr)
                                        self.newhealthArray.append(health)
                                        self.newmaxAttackArray.append(attackMax)
                                        self.newspeedArray.append(speed)
                                        self.newmanaArray.append(mana)
                                        self.newrolesArray.append(roles)
                                    }
                                }
                                print("newtypeIDs:\(self.newtypeIDs)")
                                print("newheroNamesArray:\(self.newheroNamesArray)")
                                print("newheroImagesArray:\(self.newheroImagesArray)")
                                self.typeCollectionView.reloadData()
                            }
                        }
                    case .failure(_):
                        AlertViewUtil.sharedIns.showAlertFeedback(self, messageText: "Failed get similar heroes data", actionButton1: Wordings.BTN_CLOSE, actionButton2: "")
                    }
                }
            }
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
    }
}

extension HeroDetailViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return newtypeIDs.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let padding: CGFloat =  50
        let collectionViewSize = collectionView.frame.size.width - padding

        return CGSize(width: collectionViewSize/3, height: collectionViewSize/2.5)
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell : TypeCellView = collectionView.dequeueReusableCell(withReuseIdentifier: "typeCell", for: indexPath) as! TypeCellView
        cell.lblName.text = self.newheroNamesArray[indexPath.item]
        cell.lblName.textColor = .black
        cell.lblName.font = cell.lblName.font.withSize(12)
        let linkimage = "https://api.opendota.com"+newheroImagesArray[indexPath.item]
        print("linkimage:\(linkimage)")
        let url = URL(string: linkimage)
        cell.imgType.kf.setImage(with: url)
//        cell.imgType.downloaded(from: linkimage)
        cell.imgType.clipsToBounds = true
        cell.imgType.layer.cornerRadius = cell.imgType.frame.height / 3.5
        cell.imgType.contentMode = .scaleAspectFill
        setRoundedViewWithBorder(view: cell.typeView, borderColor: Constant.sharedIns.color_grey_300, bgColor: Constant.sharedIns.color_white)
        return cell
    }

    /// Select collection view cell
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let index = indexPath.row
        let linkimage = "https://api.opendota.com"+newheroImagesArray[indexPath.item]
        let storyboard = UIStoryboard(name: Constant.sharedIns.MAIN_STORYBOARD, bundle: nil)
        let navCon = storyboard.instantiateViewController(withIdentifier: Constant.sharedIns.DETAIL_NAV) as! UINavigationController
        let vc = navCon.viewControllers.first as! HeroDetailViewController
        vc.tabBarController?.tabBar.isHidden = true
        vc.types = newtypeArray[index]
        vc.attr = newattrArray[index]
        vc.health = newhealthArray[index]
        vc.maxAttack = newmaxAttackArray[index]
        vc.speed = newspeedArray[index]
        vc.roles = newrolesArray[index]
        vc.herosImage = linkimage
        navigationController?.pushViewController(vc, animated: true)
    }
}
