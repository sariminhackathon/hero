//
//  HeroesViewController.swift
//  dota
//
//  Created by Oscar Perdanakusuma Adipati on 14/04/20.
//  Copyright © 2020 Sarimin App. All rights reserved.
//

import UIKit
import Foundation
import Alamofire
import AlamofireObjectMapper
import SwiftyJSON
import Kingfisher
import CoreData

class HeroesViewController: BaseParentController {
    @IBOutlet weak var typeView: UIView!
    @IBOutlet weak var typeCollectionView: UICollectionView!
    var heros = [HeroStats]()
    var typeID = [String]()
    var heroNames = [String]()
    var heroImages = [String]()
    var types = [String]()
    var attrs = [String]()
    var healths = [String]()
    var maxAttacks = [String]()
    var speeds = [String]()
    var mana = [String]()
    var roless = [[String]]()
    let heroPicker = UIPickerView()
    var heroList = [String]()
    var heroIdx : Int = 0
    var heroSelectedIdx : Int = 0
    var coreHeroList: [NSManagedObject] = []
    
    @IBOutlet weak var btnHeroRole: CustomUIButton!{
        didSet{
            self.setRoundedViewWithBorder(view: self.btnHeroRole, borderColor: Constant.sharedIns.color_grey_300, bgColor: Constant.sharedIns.color_white)
            self.btnHeroRole.addTarget(self, action: #selector(onClickBtnHeroRole(_:)), for: .touchUpInside)
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setEnableTapGestureOnMainView(isEnable: false)
        
        typeCollectionView.dataSource = self
        typeCollectionView.delegate = self
        typeCollectionView.register(UINib(nibName: "TypeView", bundle: nil), forCellWithReuseIdentifier: "typeCell")
        
        if(isConnectingInternet()==false){
            AlertViewUtil.sharedIns.showAlertFeedback(self, messageText: Wordings.msg_internet_error, actionButton1: Wordings.BTN_CLOSE, actionButton2: "")
            return
        } else {
            compareDataCount()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tabBarController?.tabBar.isHidden = false
    }
        
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    func compareDataCount() {
        let url = NSURL(string:Constant.getAPIHeroes())
        print("HEROES URL:\(String(describing: url!))")

        Alamofire.request(Constant.getAPIHeroes(), method : .get, parameters: nil).responseJSON { response in
            switch(response.result) {
            case .success(_):
                if response.result.value != nil {
                    let json = JSON(response.result.value as Any)
                    let statusCode = response.response?.statusCode
                    print("statusCode:\(String(describing: statusCode))")
                    print("data:\(json)")
                    CustomUserDefaults.shared.setAPICount("\(json.count)")
                    print("data count compare:\(CustomUserDefaults.shared.getAPICount())-\(CustomUserDefaults.shared.getCoreDataCount())")
                    let apiCount = "\(CustomUserDefaults.shared.getAPICount())"
                    let coredataCount = "\(CustomUserDefaults.shared.getCoreDataCount())"
                    if (apiCount != coredataCount) {
                        self.callAPIHeroes(roleas: "All")
                    } else {
                        self.getCoreHeroes(roleas: "All")
                    }
                }
            case .failure(_):
                AlertViewUtil.sharedIns.showAlertFeedback(self, messageText: "Failed get heroes data", actionButton1: Wordings.BTN_CLOSE, actionButton2: "")
            }
        }
    }

    func callAPIHeroes(roleas: String){
        typeID.removeAll()
        heroNames.removeAll()
        heroImages.removeAll()
        types.removeAll()
        attrs.removeAll()
        healths.removeAll()
        maxAttacks.removeAll()
        speeds.removeAll()
        roless.removeAll()
        heroList.removeAll()
        heroList.append("All")
        let url = NSURL(string:Constant.getAPIHeroes())
        print("HEROES URL:\(String(describing: url!))")
        showProgressDialog(message: Wordings.msg_please_wait)

        Alamofire.request(Constant.getAPIHeroes(), method : .get, parameters: nil).responseJSON { response in
            self.dismissProgressDialog()
            switch(response.result) {
            case .success(_):
                if response.result.value != nil {
                    let json = JSON(response.result.value as Any)
                    let statusCode = response.response?.statusCode
                    print("statusCode:\(String(describing: statusCode))")
                    print("data:\(json)")
                    if ((json.count) > 0) {
                        for i in 0..<(json.count)  {
                            let heroID = json[i]["id"].stringValue
                            let heroName = json[i]["localized_name"].stringValue
                            let heroAttr = json[i]["primary_attr"].stringValue
                            let attackType = json[i]["attack_type"].stringValue
                            let heroImage = json[i]["img"].stringValue
                            let health = json[i]["base_health"].stringValue
                            let attackMax = json[i]["base_attack_max"].stringValue
                            let speed = json[i]["move_speed"].stringValue
                            let mana = json[i]["base_mana"].stringValue
                            let roles = json[i]["roles"].arrayObject as! [String]
                            for rolea in roles {
                                if self.heroList.contains(rolea) {
                                    print("rolea:\(rolea)")
                                } else {
                                    self.heroList.append(rolea)
                                }
                            }
                            if (roleas == "All") {
                                self.typeID.append(heroID)
                                self.heroNames.append(heroName)
                                self.attrs.append(heroAttr)
                                self.types.append(attackType)
                                self.heroImages.append(heroImage)
                                self.healths.append(health)
                                self.maxAttacks.append(attackMax)
                                self.speeds.append(speed)
                                self.mana.append(mana)
                                self.roless.append(roles)
                            } else if roles.contains(roleas) {
                                self.typeID.append(heroID)
                                self.heroNames.append(heroName)
                                self.attrs.append(heroAttr)
                                self.types.append(attackType)
                                self.heroImages.append(heroImage)
                                self.healths.append(health)
                                self.maxAttacks.append(attackMax)
                                self.speeds.append(speed)
                                self.mana.append(mana)
                                self.roless.append(roles)
                            }
                        }
                        print("heroList:\(self.heroList)")
                        self.typeCollectionView.reloadData()
                        self.setupHeroRolePicker()
                        self.coreHeroes()
                    }
                }
            case .failure(_):
                AlertViewUtil.sharedIns.showAlertFeedback(self, messageText: "Failed get heroes data", actionButton1: Wordings.BTN_CLOSE, actionButton2: "")
            }
        }
    }
    
    func getCoreHeroes(roleas: String) {
        typeID.removeAll()
        heroNames.removeAll()
        heroImages.removeAll()
        types.removeAll()
        attrs.removeAll()
        healths.removeAll()
        maxAttacks.removeAll()
        speeds.removeAll()
        mana.removeAll()
        roless.removeAll()
        heroList.removeAll()
        heroList.append("All")
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        let managedContext = appDelegate.persistentContainer.viewContext
        let fetchRequestHeroes = NSFetchRequest<NSManagedObject>(entityName: "Hero")
        do {
            coreHeroList = try managedContext.fetch(fetchRequestHeroes)
            CustomUserDefaults.shared.setCoreDataCount("\(coreHeroList.count)")
            print("heroes.count4:\(coreHeroList.count)")
            if (coreHeroList.count > 0) {
                for hero in coreHeroList {
                    let roles = hero.value(forKey: "roles") as? [String] ?? [""]
                    for rolea in roles {
                        if self.heroList.contains(rolea) {
                            print("rolea:\(rolea)")
                        } else {
                            self.heroList.append(rolea)
                        }
                    }
                    if (roleas == "All") {
                        typeID.append(hero.value(forKey: "heroID") as? String ?? "")
                        heroNames.append(hero.value(forKey: "heroName") as? String ?? "")
                        attrs.append(hero.value(forKey: "primaryAttr") as? String ?? "")
                        types.append(hero.value(forKey: "attackType") as? String ?? "")
                        heroImages.append(hero.value(forKey: "heroImage") as? String ?? "")
                        healths.append(hero.value(forKey: "baseHealth") as? String ?? "")
                        maxAttacks.append(hero.value(forKey: "baseMaxAttack") as? String ?? "")
                        speeds.append(hero.value(forKey: "moveSpeed") as? String ?? "")
                        mana.append(hero.value(forKey: "baseMana") as? String ?? "")
                        roless.append(hero.value(forKey: "roles") as? [String] ?? [""])
                    } else if roles.contains(roleas) {
                        typeID.append(hero.value(forKey: "heroID") as? String ?? "")
                        heroNames.append(hero.value(forKey: "heroName") as? String ?? "")
                        attrs.append(hero.value(forKey: "primaryAttr") as? String ?? "")
                        types.append(hero.value(forKey: "attackType") as? String ?? "")
                        heroImages.append(hero.value(forKey: "heroImage") as? String ?? "")
                        healths.append(hero.value(forKey: "baseHealth") as? String ?? "")
                        maxAttacks.append(hero.value(forKey: "baseMaxAttack") as? String ?? "")
                        speeds.append(hero.value(forKey: "moveSpeed") as? String ?? "")
                        mana.append(hero.value(forKey: "baseMana") as? String ?? "")
                        roless.append(hero.value(forKey: "roles") as? [String] ?? [""])
                    }
                }
                print("roless:\(roless)")
                self.typeCollectionView.reloadData()
                self.setupHeroRolePicker()
            } else {
                callAPIHeroes(roleas: "All")
            }
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
    }
    
    func coreHeroes() {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        let managedContext = appDelegate.persistentContainer.viewContext
        let fetchRequestHeroes = NSFetchRequest<NSManagedObject>(entityName: "Hero")
        do {
            let heroes = try managedContext.fetch(fetchRequestHeroes)
            print("heroes.count1:\(heroes.count)-\(typeID.count)")
            if (heroes.count == 0) {
                for i in 0..<(typeID.count)  {
                    let insertHeroes = NSEntityDescription.insertNewObject(forEntityName: "Hero", into: managedContext)
                    insertHeroes.setValue(typeID[i], forKey: "heroID")
                    insertHeroes.setValue(heroNames[i], forKey: "heroName")
                    insertHeroes.setValue(attrs[i], forKey: "primaryAttr")
                    insertHeroes.setValue(types[i], forKey: "attackType")
                    insertHeroes.setValue(heroImages[i], forKey: "heroImage")
                    insertHeroes.setValue(healths[i], forKey: "baseHealth")
                    insertHeroes.setValue(maxAttacks[i], forKey: "baseMaxAttack")
                    insertHeroes.setValue(speeds[i], forKey: "moveSpeed")
                    insertHeroes.setValue(mana[i], forKey: "baseMana")
                    insertHeroes.setValue(roless[i], forKey: "roles")
                    do {
                        try managedContext.save()
                        coreHeroList.append(insertHeroes)
                    } catch let error as NSError {
                        print("Could not save. \(error), \(error.userInfo)")
                    }
                }
                CustomUserDefaults.shared.setCoreDataCount("\(coreHeroList.count)")
                print("heroes.count2:\(coreHeroList.count)")
            } else {
                let deleteRequestHero = NSBatchDeleteRequest( fetchRequest: fetchRequestHeroes as! NSFetchRequest<NSFetchRequestResult>)
                do {
                    coreHeroList.removeAll()
                    try managedContext.execute(deleteRequestHero)
                    try managedContext.save()
                    for i in 0..<(typeID.count)  {
                        let insertHeroes = NSEntityDescription.insertNewObject(forEntityName: "Hero", into: managedContext)
                        insertHeroes.setValue(typeID[i], forKey: "heroID")
                        insertHeroes.setValue(heroNames[i], forKey: "heroName")
                        insertHeroes.setValue(attrs[i], forKey: "primaryAttr")
                        insertHeroes.setValue(types[i], forKey: "attackType")
                        insertHeroes.setValue(heroImages[i], forKey: "heroImage")
                        insertHeroes.setValue(healths[i], forKey: "baseHealth")
                        insertHeroes.setValue(maxAttacks[i], forKey: "baseMaxAttack")
                        insertHeroes.setValue(speeds[i], forKey: "moveSpeed")
                        insertHeroes.setValue(mana[i], forKey: "baseMana")
                        insertHeroes.setValue(roless[i], forKey: "roles")
                        do {
                            try managedContext.save()
                            coreHeroList.append(insertHeroes)
                        } catch let error as NSError {
                            print("Could not save. \(error), \(error.userInfo)")
                        }
                    }
                    CustomUserDefaults.shared.setCoreDataCount("\(coreHeroList.count)")
                    print("heroes.count3:\(coreHeroList.count)")
                } catch let error as NSError {
                    print("Could not delete. \(error), \(error.userInfo)")
                }
            }
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
    }
    
    @objc func onClickBtnHeroRole(_ sender: UIButton){
        if heroList.count > 0{
            heroPicker.dataSource = self
            heroPicker.delegate = self
            
            btnHeroRole.becomeFirstResponder()
            heroPicker.selectRow(heroIdx, inComponent: 0, animated: false)
        }
    }
    
    func setupHeroRolePicker(){
        heroPicker.dataSource = self
        heroPicker.delegate = self
        
        let toolbar  = UIToolbar()
        toolbar.sizeToFit()
        
        let chooseButton = UIBarButtonItem(title: Wordings.BTN_CHOOSE, style: UIBarButtonItem.Style.done, target: nil, action: #selector(pickerHeroRoleDonePressed))
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        
        chooseButton.tintColor = UIColor(string: Constant.sharedIns.color_green_main)
        
        let cancelButton = UIBarButtonItem(title: Wordings.BTN_CANCEL, style: UIBarButtonItem.Style.done, target: nil, action: #selector(pickerCancelPressed))
        
        cancelButton.tintColor = UIColor(string: Constant.sharedIns.color_red_main)
        
        toolbar.setItems([cancelButton, flexSpace, chooseButton], animated: false)
        
        btnHeroRole.customInputAccessoryView = toolbar
        btnHeroRole.customInputView = heroPicker
    }
    
    @objc func pickerHeroRoleDonePressed(){
        setButtonText(button: btnHeroRole, text: heroList[heroSelectedIdx])
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        let managedContext = appDelegate.persistentContainer.viewContext
        let fetchRequestHeroes = NSFetchRequest<NSManagedObject>(entityName: "Hero")
        do {
            coreHeroList = try managedContext.fetch(fetchRequestHeroes)
            if (coreHeroList.count > 0) {
                getCoreHeroes(roleas: heroList[heroSelectedIdx])
            } else {
                callAPIHeroes(roleas: heroList[heroSelectedIdx])
            }
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
        pickerCancelPressed()
    }
        
    @objc func pickerCancelPressed(){
        btnHeroRole.resignFirstResponder()
        heroSelectedIdx = heroIdx
    }
}

extension HeroesViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return typeID.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let padding: CGFloat =  50
        let collectionViewSize = collectionView.frame.size.width - padding

        return CGSize(width: collectionViewSize/2, height: collectionViewSize/1.8)
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell : TypeCellView = collectionView.dequeueReusableCell(withReuseIdentifier: "typeCell", for: indexPath) as! TypeCellView
        cell.lblName.text = self.heroNames[indexPath.item]
        cell.lblName.textColor = .black
        let linkimage = "https://api.opendota.com"+heroImages[indexPath.item]
        print("linkimage:\(linkimage)")
        let url = URL(string: linkimage)
        cell.imgType.kf.setImage(with: url)
//        cell.imgType.downloaded(from: linkimage)
        cell.imgType.clipsToBounds = true
        cell.imgType.layer.cornerRadius = cell.imgType.frame.height / 2
        cell.imgType.contentMode = .scaleAspectFill
        setRoundedViewWithBorder(view: cell.typeView, borderColor: Constant.sharedIns.color_grey_300, bgColor: Constant.sharedIns.color_white)
        return cell
    }

    /// Select collection view cell
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let index = indexPath.row
        let linkimage = "https://api.opendota.com"+heroImages[indexPath.item]
        let storyboard = UIStoryboard(name: Constant.sharedIns.MAIN_STORYBOARD, bundle: nil)
        let navCon = storyboard.instantiateViewController(withIdentifier: Constant.sharedIns.DETAIL_NAV) as! UINavigationController
        let vc = navCon.viewControllers.first as! HeroDetailViewController
        vc.tabBarController?.tabBar.isHidden = true
        vc.types = types[index]
        vc.attr = attrs[index]
        vc.health = healths[index]
        vc.maxAttack = maxAttacks[index]
        vc.speed = speeds[index]
        vc.roles = roless[index]
        vc.herosImage = linkimage
        navigationController?.pushViewController(vc, animated: true)
    }
}

extension HeroesViewController: UIPickerViewDataSource, UIPickerViewDelegate {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return heroList.count
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        heroSelectedIdx = row
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return heroList[row]
    }
}

