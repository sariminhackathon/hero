//
//  Constant.swift
//  Arah
//
//  Created by mac on 4/5/16.
//  Copyright © 2016 Primatek Infocom. All rights reserved.
//

import UIKit
import MessageUI
import Foundation

#if DEVELOPMENT
    let uriBase_api: String = "https://api.opendota.com/api/"
    let uriTest_api: String = "http://api-samuat.tokiomarine-life.co.id/activities/"
    let uriForecast_api: String = "https://api.darksky.net/"
#elseif PRODUCTION
    let uriBase_api: String = "https://api.opendota.com/api/"
    let uriTest_api: String = "http://api-samuat.tokiomarine-life.co.id/activities/"
    let uriForecast_api: String = "https://api.darksky.net/"
#else
    let uriBase_api: String = "https://api.opendota.com/api/"
    let uriTest_api: String = "http://api-samuat.tokiomarine-life.co.id/activities/"
    let uriForecast_api: String = "https://api.darksky.net/"
#endif

let heroesStatAPI: String = "herostats"
let versionAPI: String = "v2"
let forecastAPI: String = "forecast/"
let insertPlaceAPI: String = "map/api/api.php?action=insertPlace"
let getPlaceAPI: String = "map/api/api.php?action=getPlace"
let loginAPI: String = "api.php?action=loginClient"
let registerAPI: String = "api.php?action=registerClient"
let forgotPasswordAPI: String = "pass/recovery/"
let promoAPI: String = "api.php?action=promo"
let productCategoryAPI: String = "api.php?action=getAllProductCategory"
let productListAPI: String = "api.php?action=productList"
let productAPI: String = "api.php?action=getAllProduct"
let similarProductAPI: String = "api.php?action=getSimilarProduct"
let productSalesAPI: String = "api.php?action=productSales"
let updateAccountAPI: String = "api.php?action=updateClient"
let agentProfileAPI: String = "agencies/agent/profile/"
let agentSPAJlistAPI: String = "policies/spaj/list/"
let agentSPAJInfoAPI: String = "policies/spaj/info/"
let refreshTokenAPI: String = "auth/token_sam/refresh/"
let sendFCMTokenURL: String = "sendFCMToken"
let uploadProfilePictureURL: String = "uploadProfilePicture"
let sentContact: String = "contact/"
let sentActivity: String = "activity/"
let sentCase: String = "case/"
let sentChildren: String = "children/"
let sentBrother: String = "brother/"
let sentSister: String = "sister/"
let sentPhone: String = "phone/"
let sentEmail: String = "email/"
let sentMedicalExpenses: String = "medical_expenses/"
let sentOldAgeFunds: String = "old_age_fund/"
let sentNecessityIncome: String = "necessity_income/"
let sentEducationFund: String = "education_fund/"
let sentDebts: String = "debts/"
let sentCI: String = "ci/"
let freePA: String = "free_pa/candidate/"
let moneyReport: String = "sam/money_analysis_report/"

class Constant {
    
    static let sharedIns = Constant()
    
    //FONT SIZE
    let XX_SMALL_FONT_SIZE : CGFloat = 8
    let X_SMALL_FONT_SIZE : CGFloat = 10
    let SMALL_FONT_SIZE : CGFloat = 12
    let NORMAL_FONT_SIZE : CGFloat = 14
    let MEDIUM_NORMAL_FONT_SIZE : CGFloat = 16
    let MEDIUM_FONT_SIZE : CGFloat = 18
    let MEDIUM_LARGE_FONT_SIZE : CGFloat = 20
    let LARGE_FONT_SIZE : CGFloat = 22
    let X_LARGE_FONT_SIZE : CGFloat = 24
    let XX_LARGE_FONT_SIZE : CGFloat = 26
    let XXX_LARGE_FONT_SIZE : CGFloat = 28
    let SUPER_LARGE_FONT_SIZE : CGFloat = 34
    let HYPER_LARGE_FONT_SIZE : CGFloat = 40
    
    
    //COLORS CODE
    //RED
    let color_red_500 : String = "#F44336"
    //let color_red_main: String = "#e80c00"
    let color_red_main : String = "#E11931"
    let color_red_maroon : String = "#800000"
    
    
    //ORANGE
    let color_orange_200: String = "#ffcc80"
    let color_orange_300: String = "#ffb74d"
    let color_orange_400: String = "#ffa726"
    let color_orange_500: String = "#ff9800"
    let color_orange_600: String = "#FB8C00"
    let color_orange_700: String = "#f57c00"
    let color_orange_800: String = "#ef6c00"
    let color_orange_900: String = "#e65100"
    let color_orange_main : String = "FF9900"
    
    //GREEN
    let color_green_500: String = "#34B44A"
    let color_green_700: String = "#388e3c"
    let color_green_800: String = "#2e7d32"
    let color_green_common: String = "#33B04A"
    let color_green_main : String = "#33b44a"
    
    //GRAY
    
    let color_grey_100: String = "#F5F5F5"
    let color_grey_200: String = "#EEEEEE"
    let color_grey_300: String = "#E0E0E0"
    let color_grey_400: String = "#CBCBCB"
    let color_grey_500: String = "#9E9E9E"
    let color_grey_600: String = "#757575"
    let color_grey_700: String = "#616161"
    let color_grey_800: String = "#424242"
    let color_grey_300_alpha: String = "#1A9E9E9E"
    let color_grey_400_alpha: String = "#80E0E0E0"
    let color_grey_500_alpha: String = "#809E9E9E"
    let color_grey_800_alpha: String = "#80424242"
    let color_grey_gainsboro: String = "#DCDCDC"
    
    
    //BLACK
    let color_black : String = "000000"
    
    //WHITE
    let color_white: String = "#FFFFFF"
    let color_white_alpha_70 : String = "#B3FFFFFF"
    let color_white_alpha_60 : String = "#99FFFFFF"
    let color_white_alpha_50 : String = "#80FFFFFF"
    let color_white_alpha_40 : String = "#66FFFFFF"
    let color_white_alpha_30 : String = "#4DFFFFFF"
    
    //BLUE
    let color_blue : String = "0097a8"
    let color_blue_main : String = "#007AFF"
    
    //FLEXIFAST
    let color_flexi_fast: String = "#81b7dc"
    
    //PUSH MESSAGE
    let color_promotion = "#5ca9df"
    let color_payment = "#FB8C00"
    let color_survey = "#1aa38c"
    let color_quiz = "#b969c7"
    
    
    let INITIAL_STORYBOARD = "Initial"
    let MAIN_STORYBOARD = "Main"
    let HOMEPAGE_STORYBOARD = "Homepage"
    let PRODUCT_STORYBOARD = "Product"
    let CART_STORYBOARD = "Cart"
    let OTHERS_STORYBOARD = "Others"
    let CONTRACT_STORYBOARD = "ContractDetail"
    let LOCKSCREEN_STORYBOARD = "LockScreen"
    let MESSAGE_STORYBOARD = "MessageDetailandSettings"
    let INITIAL_NAV_ID = "InitialNavigation"
    let NAV_DRAWER_ID = "DrawerController"
    let HOMEPAGE_ID = "HomepageID"
    let ACTIVITY_POPUP_ID = "ActivityPopUpID"
    let NOTES_POPUP_ID = "NotesPopUpID"
    let CASE_COLLECTIONVIEW_HEADER_ID = "caseCollectionViewHeaderID"
    let FLEXI_FAST_STORYBOARD = "FlexiFast"
    let CONTACT_STORYBOARD = "Contact"
    let SCHEDULE_STORYBOARD = "Schedule"
    let DEAL_STORYBOARD = "Deal"
    let AGENT_STORYBOARD = "Agent"
    let FLEXI_FAST_FORM_STORYBOARD = "FlexiFastApplicationForm"
    let TYPE_NAV = "TypeNav"
    let DETAIL_NAV = "DetailNav"
    let PRODUCT_LIST_NAV = "ProductListNav"
    let PRODUCT_DETAIL_NAV = "ProductDetailNav"
    let CART_NAV = "cartNav"
    let CREATE_EDIT_CONTACT_NAV = "CreateEditContactNav"
    let SCHEDULE_LIST_NAV = "ScheduleListNav"
    let CREATE_EDIT_CASE_NAV = "CreateEditCaseNav"
    let CREATE_EDIT_ACTIVITY_NAV = "CreateEditActivityNav"
    let ACTIVITY_POPUP_NAV = "ActivityPopUpNav"
    let SCHEDULE_NAV = "ScheduleNav"
    let CONTACT_PICKER_NAV = "ContactPickerNav"
    let ACTIVE_CONTACT_PICKER_NAV = "ActiveContactPickerNav"
    let AGENT_PROFILE_NAV = "AgentProfileNav"
    let OPEN_PROSPECT_NAV = "OpenProspectNav"
    let SPAJ_LIST_NAV = "SPAJListNav"
    let SPAJ_DETAIL_NAV = "SPAJDetailNav"
    let OPEN_CASE_NAV = "OpenCaseNav"
    let OPEN_MEETING_NAV = "OpenMeetingNav"
    let ANNOUNCEMENT_NAV = "AnnouncementNav"
    let SETTINGS_NAV = "SettingsNav"
    let BROWSER_NAV = "browserNav"
    let PLACE_NAV = "placeNav"
    let ABOUT_NAV = "aboutNav"
    let ACCESS_NAV = "AccessNav"
    let PRIVACY_POLICY_NAV = "PrivacyPolicyNav"
    let ATTRIBUTION_NAV = "AttributionNav"
    let WEBVIEW_NAV = "WebViewNav"
    let NEARBYPLACENONAR_NAV = "NearbyPlaceNonARNav"
    let FLEXI_FAST_OFFER_NAV = "FlexiFastOfferNav"
    let FLEXI_FAST_OFFER_SIMULATION_NAV = "FlexiFastOfferSimulationNav" 
    let FLEXI_FAST_SIMULATION_NAV = "FlexiFastSimulationNav"
    let FLEXI_FAST_PARENT_APP_FORM = "FlexiFastParentForm"
    let FLEXI_FAST_SIMULATION = "FlexiFastSimulation"
    let FLEXI_FAST_APP_FROM_2 = "FlexiFastForm2"
    let FLEXI_FAST_APP_FROM_3 = "FlexiFastForm3"
    let FLEXI_FAST_APP_FROM_4 = "FlexiFastForm4"
    let FLEXI_FAST_APP_FROM_5 = "FlexiFastForm5"
    let FLEXI_FAST_APP_FROM_6 = "FlexiFastForm6"
    let HOMEPAGE_NAV = "HomepageNav"
    let CHANGE_PIN = "ChangePINNav"
    let CONTACT_ME = "ContactMeNav"
    let POS_LOCATION_NAV = "StoreLocationNav"
    let LOCK_SCREENNAV = "lockscreenNav"
    let MY_PROFILE = "MyProfile"
    let LOCK_SCREEN = "Lockscreen"
    let CONTRACT_DETAIL_NAV = "ContractNav"
    let INSTALLMENT_ID = "InstallmentSchedule"
    
    let INST_SCHEDULE_NAV = "InstallmentScheduleNav"
    let CONTRACT_NAV = "ContractNav"
    let REGISTER_NAV = "RegisterFormNav"
    let FORGOT_PIN_NAV = "ForgotPinNav"
    let HOW_TO_PAY_NAV = "HowToPayNewNav"
    let PAYMENT_LOCATION_NAV = "PaymentLocationNav"
    let E_SHOPS_NAV = "e-ShopsNav"
    let OUR_GUARANTEE_NAV = "OurGuaranteeNav"
    let GENERAL_SIMULATION_NAV = "GeneralSimulationNav"
    let MESSAGE_SETTINGS_NAV = "MessageSettingsNav"
    let MESSAGE_DETAIL_NAV = "MessageDetailNav"
    
    
    let SEGUE_PERSONAL_DETAIL = "personalDetail"
    let SEGUE_HOME = "home"
    let SEGUE_INBOX = "inbox"
    let SEGUE_CONTACT_US = "contactUs"
    let SEGUE_HOW_TO_PAY = "howToPay"
    let SEGUE_MY_CONTRACT = "myContract"
    let SEGUE_HOW_TO_APPLY = "howToApply"
    let WEBVIEW_ID = "WebViewNavController"
    let MESSAGE_DETAIL = "MessageDetailNav"
    let LOGIN_NAV = "LoginNav"
    
    //APP
    let instagramApp = "instagram://user?username=homecreditid"
    let fbApp = "fb://profile/523957857647302"
    let twitterApp = "twitter://user?screen_name=homecreditid_"
    let linkedinApp = "linkedin://company/homecreditindonesia"
    
    //URL
    let URL_TERMS_CONDITION = ""
    let URL_FB_HCID = ""
    let URL_TWITTER_HCID = ""
    let URL_INSTAGRAM_HCID = ""
    let URL_LINKEDIN_HCID = ""
    let URL_WEBSITE_HCID = ""
    let URL_MAIN_WEB = ""
    let URL_EASY_COVER = ""
    let URL_EASY_COVER_CLAIM = ""
    let URL_PROTECTION = ""
    let URL_PROTECTION_MAIN = ""
    let URL_PROMO = ""
    
    //CONTRACT DETAIL
    let CONTRACT_ACTIVE : String = "Active"
    let CONTRACT_FINISHED : String = "Finished"
    let CONTRACT_APPROVED : String = "Approved"
    let CONTRACT_CANCELLED : String = "Cancelled"
    let CONTRACT_REJECTED : String = "Rejected"
    let CONTRACT_SIGNED : String = "Signed"
    let CONTRACT_IN_PROCESS : String = "In Process"
    let CONTRACT_IN_PRE_PROCESS : String = "In Preprocess"
    let CONTRACT_PRE_APPROVED : String = "Pre-approved"
    let CONTRACT_PAID_OFF : String = "Paid off"
    let CONTRACT_WRITE_OFF : String = "Written off"
    let SERVICE_GIFT_PAYMENT_1 : String = "GIFT-1"
    let SERVICE_GIFT_PAYMENT_2 : String = "GIFT-2"
    let SERVICE_ADLD : String = "ADLD"
    let SERVICE_AMAN : String = "AMAN"
    let SERVICE_ACTIVE : String = "ACTIVE"
    let SERVICE_ELIGIBLE : String = "ELIGIBLE"
    let PRODUCT_MPF : String = "MPF"
    
    let FIREBASE_PROMOTION = "CT01"
    let FIREBASE_PAYMENT = "CT02"
    let FIREBASE_CONTRACT_DATA = "CT03"
    let FIREBASE_GENERAL_INFO = "CT04"
    let FIREBASE_SURVEY = "CT05"
    let FIREBASE_QUIZ = "CT06"

    class func getAPIHeroes() -> String {
        let uri = uriBase_api + heroesStatAPI
        return uri
    }
    
    class func getAPIForecastIO() -> String {
        let uri = uriForecast_api + forecastAPI
        return uri
    }
    
    class func getAPIInsertPlace() -> String {
        let uri = uriBase_api + insertPlaceAPI
        return uri
    }
    
    class func getAPIFilterPlace() -> String {
        let uri = uriBase_api + getPlaceAPI
        return uri
    }
    
    class func getAPILogin() -> String {
        let uri = uriBase_api + loginAPI
        return uri
    }
    
    class func getAPIRegister() -> String {
        let uri = uriBase_api + registerAPI
        return uri
    }
    
    class func getAPIProductList() -> String {
        let uri = uriBase_api + productListAPI
        return uri
    }
    
    class func getAPIProduct() -> String {
        let uri = uriBase_api + productAPI
        return uri
    }
    
    class func getAPISimilarProduct() -> String {
        let uri = uriBase_api + similarProductAPI
        return uri
    }
    
    class func getAPIPromo() -> String {
        let uri = uriBase_api + promoAPI
        return uri
    }
    
    class func getAPIOrderHistory() -> String {
        let uri = uriBase_api + productSalesAPI
        return uri
    }
    
    class func getAPIUpdateAccount() -> String {
        let uri = uriBase_api + updateAccountAPI
        return uri
    }
    
    class func getAPIProductCategory() -> String {
        let uri = uriBase_api + productCategoryAPI
        return uri
    }
    
    class func getAPIRefreshToken() -> String {
        let uri = uriBase_api + refreshTokenAPI
        return uri
    }
    
    class func getAPIForgotPassword() -> String {
        let uri = uriBase_api + forgotPasswordAPI
        return uri
    }
    
    class func getAPIAgentProfile() -> String {
        let uri = uriBase_api + agentProfileAPI
        return uri
    }
    
    class func getAPIAgentSPAJList() -> String {
        let uri = uriBase_api + agentSPAJlistAPI
        return uri
    }
    
    class func getAPIAgentSPAJInfo() -> String {
        let uri = uriBase_api + agentSPAJInfoAPI
        return uri
    }
    
    class func getAPIrefreshToken() -> String {
        let uri = uriBase_api + refreshTokenAPI
        return uri
    }
    
    class func getAPIFreePA() -> String {
        let uri = uriBase_api + freePA
        return uri
    }
    
    class func getAPIMONEYReport() -> String {
        let uri = uriBase_api + moneyReport
        return uri
    }
    
    class func sendFCMToken() -> String{
        let uri = uriBase_api + sendFCMTokenURL
        return uri
    }
    
    class func getProfilePic() -> String{
        let uri = "\(uriBase_api)getProfilePicture/"
        return uri
    }

    class func uploadProfilePicture() -> String {
        let uri = uriBase_api + uploadProfilePictureURL
        return uri
    }
    
    class func sentContactSAMDB() -> String {
        let uri = uriTest_api + sentContact
        return uri
    }
    
    class func sentActivitySAMDB() -> String {
        let uri = uriTest_api + sentActivity
        return uri
    }
    
    class func sentCaseSAMDB() -> String {
        let uri = uriTest_api + sentCase
        return uri
    }
    
    class func sentChildrenSAMDB() -> String {
        let uri = uriTest_api + sentChildren
        return uri
    }
    
    class func sentBrotherSAMDB() -> String {
        let uri = uriTest_api + sentBrother
        return uri
    }
    
    class func sentSisterSAMDB() -> String {
        let uri = uriTest_api + sentSister
        return uri
    }
    
    class func sentPhoneSAMDB() -> String {
        let uri = uriTest_api + sentPhone
        return uri
    }
    
    class func sentEmailSAMDB() -> String {
        let uri = uriTest_api + sentEmail
        return uri
    }
    
    class func sentMedicalExpensesSAMDB() -> String {
        let uri = uriTest_api + sentMedicalExpenses
        return uri
    }
    
    class func sentOldAgeFundsSAMDB() -> String {
        let uri = uriTest_api + sentOldAgeFunds
        return uri
    }
    
    class func sentNecessityIncomeSAMDB() -> String {
        let uri = uriTest_api + sentNecessityIncome
        return uri
    }
    
    class func sentEducationFundSAMDB() -> String {
        let uri = uriTest_api + sentEducationFund
        return uri
    }
    
    class func sentDebtsSAMDB() -> String {
        let uri = uriTest_api + sentDebts
        return uri
    }
    
    class func sentCISAMDB() -> String {
        let uri = uriTest_api + sentCI
        return uri
    }
    
    class func convertMilisecondToDays(milliseconds: String) -> String {
        var res = ""
        
        if milliseconds == "2592000000" {
            res = "30"
        }
        else if milliseconds == "1209600000" {
            res = "14"
        }
        else if milliseconds == "604800000" {
            res = "7"
        }
        else if milliseconds == "86400000" {
            res = "1"
        }
        
        
        return "\(res)"
    }
    
    class func convertDateStringFormat(date: Date, format: String) -> String {
        
        let dateConvert = date
        let dayTimePeriodFormatter = DateFormatter()
        dayTimePeriodFormatter.dateFormat =  format  //"MMMM yyyy"    //"EEEE, h a"
        
        let dateStr = dayTimePeriodFormatter.string(from: dateConvert as Date)
        
        return dateStr
    }
    
    class func getDateNow(format: String) -> String {
        
        let dateConvert = Date()
        let dayTimePeriodFormatter = DateFormatter()
        dayTimePeriodFormatter.dateFormat =  format  //"MMMM yyyy"    //"EEEE, h a"
        
        let dateStr = dayTimePeriodFormatter.string(from: dateConvert as Date)
        
        return dateStr
    }
    
    class func convertStringToDate(dateString: String!) -> Date {
        let dateFormatter = DateFormatter()
        //dateFormatter.timeZone = NSTimeZone(abbreviation: "WIB")! as TimeZone
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.dateFormat = "yyyy-MM-dd"
        print("dateString \(dateString)")
        
        let date: Date = dateFormatter.date(from: dateString)! as Date
        
        return date
    }
    
    class func convertStringToDatewithFormat(dateString: String!, format: String) -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.dateFormat = format
        dateFormatter.locale = Locale(identifier: "en_GB")
        print("dateString \(dateString!)")
        let date: Date = dateFormatter.date(from: dateString!)! as Date
        print("convertStringToDatewithFormat:\(date)")
        return date
    }
    
    func setStringToServerDate(dateString: String) -> Date{
        //Server date as GMT
        let serverDateFormat = DateFormatter()
        serverDateFormat.dateFormat = "EEE, dd MMM yyyy HH:mm:ss Z"
        serverDateFormat.timeZone = TimeZone(abbreviation: "GMT")
        
        //Convert to local timezone
        guard let date = serverDateFormat.date(from: dateString) else{
            return Date.init()
        }
        
        let localDateFormat = DateFormatter()
        localDateFormat.dateFormat = "dd/MM/yyyy"
        localDateFormat.timeZone = TimeZone.current
        
        let localDateString = localDateFormat.string(from: date as Date)
        
        guard let localDate = localDateFormat.date(from: localDateString) else {
            return Date.init()
        }
        
        print("DATE_TEMP: \(localDateString)")
        
        return localDate as Date
    }
    
    class func changeToLocalDate(dateString: String!) -> String{
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        let date = dateFormatter.date(from: dateString!)
        
        let outputDatedateFormatter = DateFormatter()
        outputDatedateFormatter.dateFormat = "dd MMMM yyyy"
        //leave the time zone at the default (user's time zone)
        let displayString = outputDatedateFormatter.string(from: date!)

        return displayString
    }
    
    class func convertStringToMonthYear(dateString: String) -> String {
        
        let dateConvert = convertStringToDate(dateString: dateString)
        let dayTimePeriodFormatter = DateFormatter()
        dayTimePeriodFormatter.dateFormat = "MMMM yyyy"    //"EEEE, h a"
        
        let dateStr = dayTimePeriodFormatter.string(from: dateConvert as Date)
        
        return dateStr
    }
    
    class func convertStringToMonthOnly(dateString: String) -> String {
        
        let dateConvert = convertStringToDate(dateString: dateString)
        let dayTimePeriodFormatter = DateFormatter()
        dayTimePeriodFormatter.dateFormat = "MMMM"    //"EEEE, h a"
        
        let dateStr = dayTimePeriodFormatter.string(from: dateConvert as Date)
        
        return dateStr
    }
    
    class func convertDateStringtoAnotherFormat(dateString: String, fromFormat: String, toFormat: String) -> String {
        let inputDateFormatter = DateFormatter()
        inputDateFormatter.dateFormat = fromFormat
//        inputDateFormatter.locale = Locale(identifier: "id_ID")
        let date = inputDateFormatter.date(from: dateString)
        
        let outputDateFormatter = DateFormatter()
        outputDateFormatter.dateFormat = toFormat
        let result = outputDateFormatter.string(from: date!)
        return result
    }
    
    class func countAge(birthday: String!) -> Int {
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.dateFormat = "dd-MM-yyyy"
        print("birthday: \(birthday)")
        let birthdate: Date = dateFormatter.date(from: birthday)! as Date
        let now = Date()
        let birthdays: Date = birthdate
        let calendar = Calendar.current
        let ageComponents = calendar.dateComponents([.year], from: birthdays, to: now)
        let age = ageComponents.year!
        
        return age
    }
    
    class func currentTimeMillis() -> Int64{
        let nowDouble = NSDate().timeIntervalSince1970
        return Int64(nowDouble*1000) + Int64(nowDouble/1000)
    }
    
    class func checkDeviceIsIpad() -> Bool {
        var result = false
        if UIDevice.current.userInterfaceIdiom == .pad {
            result = true
        }
        /*
        UIDevice.currentDevice().userInterfaceIdiom == .Phone
        UIDevice.currentDevice().userInterfaceIdiom == .Unspecified
        */
        return result
    }
    
    /*class func getDeviceImei() -> String {
        return UIDevice.currentDevice.identifierForVendor!.UUIDString
    }*/
    class func configuredMailComposeViewController(delegate: MFMailComposeViewControllerDelegate) -> MFMailComposeViewController {
        let mailComposerVC = MFMailComposeViewController()
        mailComposerVC.mailComposeDelegate = delegate //self // Extremely important to set the --mailComposeDelegate-- property, NOT the --delegate-- property
        
        mailComposerVC.setToRecipients(["someone@somewhere.com"])
        mailComposerVC.setSubject("Sending you an in-app e-mail...")
        mailComposerVC.setMessageBody("Sending e-mail in-app is not so bad!", isHTML: false)
        
        return mailComposerVC
    }
    
    class func showSendMailErrorAlert() {
        let sendMailErrorAlert = UIAlertView(title: "Could Not Send Email",
            message: "Your device could not send e-mail.  Please check e-mail configuration and try again.",
            delegate: self, cancelButtonTitle: "OK")
        sendMailErrorAlert.show()
    }
    // ---------------------------------------------------------
    class func getMainStoryboard() -> UIStoryboard {
        let storyboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil);
        
        return storyboard
    }
    class func uicolorFromHex(rgbValue:UInt32)->UIColor{
        let red = CGFloat((rgbValue & 0xFF0000) >> 16)/256.0
        let green = CGFloat((rgbValue & 0xFF00) >> 8)/256.0
        let blue = CGFloat(rgbValue & 0xFF)/256.0
        
        return UIColor(red:red, green:green, blue:blue, alpha:1.0)
    }
    
    class func sizeToFillWidthOfSize(size:CGSize) -> CGSize {
        
        let imageSize = CGSize(width: 110, height: 110)
        var returnSize = size
        
        let aspectRatio = imageSize.width / imageSize.height
        
        returnSize.height = returnSize.width / aspectRatio
        
        if returnSize.height > size.height {
            returnSize.height = size.height
            returnSize.width = size.height * aspectRatio
        }
        
        return returnSize
    }
    class func isValidEmail(testStr:String) -> Bool {
        print("validate emilId: \(testStr)")
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        let result = emailTest.evaluate(with: testStr)
        return result
    }
    class func isValidPhoneNumber(value: String) -> Bool {
        //let PHONE_REGEX = "^\\d{4}-\\d{8}$"
        let PHONE_REGEX = "^((\\+)|(62))[0-9]{11,14}$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
        let result =  phoneTest.evaluate(with: value)
        return result
    }
    class func currencyNominal(value: Int) -> String {
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = NumberFormatter.Style.decimal
        numberFormatter.locale = Locale(identifier: "id_ID")
        let formattedNumber = numberFormatter.string(from: NSNumber(value:value))
        return formattedNumber!
    }
    class func verifyUrl (urlString: String?) -> Bool {
        if let urlString = urlString {
            if let url  = NSURL(string: urlString) {
                return UIApplication.shared.canOpenURL(url as URL)
            }
        }
        return false
    }
    class func getDayOfWeek()->Int? {
        let todayDate = NSDate()
        let myCalendar = NSCalendar(calendarIdentifier: NSCalendar.Identifier(rawValue: NSGregorianCalendar))
        let myComponents = myCalendar?.components(.weekday, from: todayDate as Date)
        let weekDay = myComponents?.weekday
        return weekDay
    }
    class func GUIDString() -> NSString {
        let newUniqueID = CFUUIDCreate(kCFAllocatorDefault)
        let newUniqueIDString = CFUUIDCreateString(kCFAllocatorDefault, newUniqueID);
        let guid = newUniqueIDString as! NSString
        
        return guid.lowercased as NSString
    }
    class func getDeviceImei() -> String {
        return (UIDevice.current.identifierForVendor?.uuidString)!
    }
    class func colorWithHexString (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString = (cString as NSString).substring(from: 1)
        }
        
        if (cString.count != 6) {
            return UIColor.gray
        }
        
        var rString = (cString as NSString).substring(to: 2)
        var gString = ((cString as NSString).substring(from: 2) as NSString).substring(to: 2)
        var bString = ((cString as NSString).substring(from: 4) as NSString).substring(to: 2)
        
        var r:CUnsignedInt = 0, g:CUnsignedInt = 0, b:CUnsignedInt = 0;
        Scanner(string: rString).scanHexInt32(&r)
        Scanner(string: gString).scanHexInt32(&g)
        Scanner(string: bString).scanHexInt32(&b)
        
        
        return UIColor(red: CGFloat(r) / 255.0, green: CGFloat(g) / 255.0, blue: CGFloat(b) / 255.0, alpha: CGFloat(1))
    }
    class func deviceRemainingFreeSpaceInBytes() -> Int64? {
        let documentDirectory = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).last!
        guard
            let systemAttributes = try? FileManager.default.attributesOfFileSystem(forPath: documentDirectory),
            let freeSize = systemAttributes[.systemFreeSize] as? NSNumber
            else {
                // something failed
                return nil
        }
        return freeSize.int64Value
    }
    class func getTotalSize() -> Int64?{
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        if let dictionary = try? FileManager.default.attributesOfFileSystem(forPath: paths.last!) {
            if let freeSize = dictionary[FileAttributeKey.systemSize] as? NSNumber {
                return freeSize.int64Value
            }
        }else{
            print("Error Obtaining System Memory Info:")
        }
        return nil
    }
    
    enum BackgroundLayout: String {
        case HeaderLogo = "logo" //"HeaderLogo850Small"
        //case BackgroundLogin = "LoginBackgroundSmall"
        case HeaderLogoLandscape = "logoLandscape"
        case BackgroundLoginSignUp = "bgSignup"
        case BackgroundOrange = "headerOrange"
        case BackgroundOrangeLandscape = "orangeLandscape"
        case BackgroundLandscapeSignUp = "bgSignupLandscape"
        case BackgroundLandscapeSplash = "bgSplashLandscape"
        case HeaderResult = "headerResult"
        case HeaderResultLandscape = "headerResultL"
        
    }
}

