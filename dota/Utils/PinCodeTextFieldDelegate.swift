//
//  PinCodeTextFieldDelegate.swift
//  PinCodeTextField
//
//  Created by Alexander Tkachenko on 3/20/17.
//  Copyright © 2017 organization. All rights reserved.
//

import Foundation

public protocol PinCodeTextFieldDelegate: class {
    func pinFieldShouldBeginEditing(_ textField: PinCodeTextField) -> Bool // return false to disallow editing.
    
    func pinFieldDidBeginEditing(_ textField: PinCodeTextField) // became first responder
    
    func pinFieldValueChanged(_ textField: PinCodeTextField) // text value changed
    
    func pinFieldShouldEndEditing(_ textField: PinCodeTextField) -> Bool // return true to allow editing to stop and to resign first responder status at the last character entered event. NO to disallow the editing session to end
    
    func pinFieldDidEndEditing(_ textField: PinCodeTextField) // called when pinCodeTextField did end editing
    
    func pinFieldShouldReturn(_ textField: PinCodeTextField) -> Bool // called when 'return' key pressed. return false to ignore.
}

/// default
public extension PinCodeTextFieldDelegate {
    func pinFieldShouldBeginEditing(_ textField: PinCodeTextField) -> Bool {
        return true
    }
    
    func pinFieldDidBeginEditing(_ textField: PinCodeTextField) {
        
    }
    
    func pinFieldValueChanged(_ textField: PinCodeTextField) {
        
    }
    
    func pinFieldShouldEndEditing(_ textField: PinCodeTextField) -> Bool {
        return true
    }
    
    func pinFieldDidEndEditing(_ textField: PinCodeTextField) {
        
    }
    
    func pinFieldShouldReturn(_ textField: PinCodeTextField) -> Bool {
        return true
    }
    
}

