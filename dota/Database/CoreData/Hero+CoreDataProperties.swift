//
//  Hero+CoreDataProperties.swift
//  dota
//
//  Created by Oscar Perdanakusuma Adipati on 18/04/20.
//  Copyright © 2020 Sarimin App. All rights reserved.
//
//

import Foundation
import CoreData


extension Hero {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Hero> {
        return NSFetchRequest<Hero>(entityName: "Hero")
    }

    @NSManaged public var attackType: String?
    @NSManaged public var baseHealth: String?
    @NSManaged public var baseMaxAttack: String?
    @NSManaged public var heroID: String?
    @NSManaged public var heroImage: String?
    @NSManaged public var heroName: String?
    @NSManaged public var moveSpeed: String?
    @NSManaged public var primaryAttr: String?
    @NSManaged public var roles: [[String]]?
    @NSManaged public var baseMana: String?

}
